(define-package "dashboard" "20210926.510" "A startup screen extracted from Spacemacs"
  '((emacs "25.3"))
  :commit "b40d342c513fd17743fa35c5820435a4ccfad2bb" :authors
  '(("Rakan Al-Hneiti" . "rakan.alhneiti@gmail.com"))
  :maintainer
  '("Jesús Martínez" . "jesusmartinez93@gmail.com")
  :keywords
  '("startup" "screen" "tools" "dashboard")
  :url "https://github.com/emacs-dashboard/emacs-dashboard")
;; Local Variables:
;; no-byte-compile: t
;; End:
