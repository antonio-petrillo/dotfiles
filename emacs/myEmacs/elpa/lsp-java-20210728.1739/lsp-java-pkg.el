(define-package "lsp-java" "20210728.1739" "Java support for lsp-mode"
  '((emacs "25.1")
    (lsp-mode "6.0")
    (markdown-mode "2.3")
    (dash "2.18.0")
    (f "0.20.0")
    (ht "2.0")
    (request "0.3.0")
    (treemacs "2.5")
    (dap-mode "0.5"))
  :commit "7ba9c459a484a9ef8d6a59e509c03d761fccde45" :keywords
  '("languague" "tools")
  :url "https://github.com/emacs-lsp/lsp-java")
;; Local Variables:
;; no-byte-compile: t
;; End:
