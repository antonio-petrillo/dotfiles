(define-package "lsp-ui" "20211003.402" "UI modules for lsp-mode"
  '((emacs "26.1")
    (dash "2.18.0")
    (lsp-mode "6.0")
    (markdown-mode "2.3"))
  :commit "2b645c62d93be4708c7b01a3618969f601d7fdb4" :authors
  '(("Sebastien Chapuis <sebastien@chapu.is>, Fangrui Song" . "i@maskray.me"))
  :maintainer
  '("Sebastien Chapuis <sebastien@chapu.is>, Fangrui Song" . "i@maskray.me")
  :keywords
  '("languages" "tools")
  :url "https://github.com/emacs-lsp/lsp-ui")
;; Local Variables:
;; no-byte-compile: t
;; End:
