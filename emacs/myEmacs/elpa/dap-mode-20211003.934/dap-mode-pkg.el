(define-package "dap-mode" "20211003.934" "Debug Adapter Protocol mode"
  '((emacs "26.1")
    (dash "2.18.0")
    (lsp-mode "6.0")
    (bui "1.1.0")
    (f "0.20.0")
    (s "1.12.0")
    (lsp-treemacs "0.1")
    (posframe "0.7.0")
    (ht "2.3"))
  :commit "579778c8a329cf01893720c36f3a0c6575288e84" :authors
  '(("Ivan Yonchovski" . "yyoncho@gmail.com"))
  :maintainer
  '("Ivan Yonchovski" . "yyoncho@gmail.com")
  :keywords
  '("languages" "debug")
  :url "https://github.com/emacs-lsp/dap-mode")
;; Local Variables:
;; no-byte-compile: t
;; End:
