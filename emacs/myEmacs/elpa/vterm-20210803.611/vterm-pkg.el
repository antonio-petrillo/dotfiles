(define-package "vterm" "20210803.611" "Fully-featured terminal emulator"
  '((emacs "25.1"))
  :commit "0ec407841ddc81fa4546abe115c12eb7c6c71521" :authors
  '(("Lukas Fürmetz" . "fuermetz@mailbox.org"))
  :maintainer
  '("Lukas Fürmetz" . "fuermetz@mailbox.org")
  :keywords
  '("terminals")
  :url "https://github.com/akermu/emacs-libvterm")
;; Local Variables:
;; no-byte-compile: t
;; End:
