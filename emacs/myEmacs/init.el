(setq gc-cons-threshold (* 100 1024 1024))

;; Initialize package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(require 'use-package)
(setq use-package-always-ensure t)

(setq inhibit-startup-message t) ;; disabling the startup buffer
(scroll-bar-mode -1)             ;; disable scrollbar
(menu-bar-mode -1)               ;; disable menubar
(tool-bar-mode -1)               ;; disable toolbar
(tooltip-mode -1)                ;; disable tooltips 
(set-fringe-mode 5)

;; (setq visible-bell nil)            ;; disabling annoying

(column-number-mode)
(global-display-line-numbers-mode t)


;;(set-face-attribute 'default nil :font "Fira Code Retina" :height 120)
(set-face-attribute 'default nil :font "JetBrainsMono Nerd Font Mono" :height 120)

;; Set the fixed pitch face
;;(set-face-attribute 'fixed-pitch nil :font "Fira Code Retina" :height 120)
(set-face-attribute 'fixed-pitch nil :font "JetBrainsMono Nerd Font Mono" :height 120)

;; Set the variable pitch face
;;(set-face-attribute 'variable-pitch nil :font "Fira Code Retina" :height 140 :weight 'regular)
(set-face-attribute 'variable-pitch nil :font "JetBrainsMono Nerd Font Mono" :height 140 :weight 'regular)

;; don't create lockfile, I know what file I'm gonna edit
(setq create-lockfiles nil)

;; don't make backup files
(setq make-backup-files nil)

;; username and email
(setq user-full-name "Antonio Petrillo"
      user-mail-address "antonio.petrillo4@studenti.unina.it")

;; type yes or no require to much effort
(fset 'yes-or-no-p 'y-or-n-p)

;; scroll one line at a time (less "jumpy" than defaults)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time

(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
  
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
  
(setq scroll-step 1) ;; keyboard scroll one line at a time

(global-unset-key (kbd "C-x C-z"))
(global-unset-key (kbd "C-z"))
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(setq vc-follow-symlinks t)

(use-package auto-package-update
  :custom
  (auto-package-update-interval 5)
  (auto-package-update-prompt-before-update t)
  (auto-package-update-hide-results t)
  ;; (auto-package-update-delete-old-version t) ;; uncomment this to delete old version
  :config
  (auto-package-update-maybe)
  (auto-package-update-at-time "09:00"))

(use-package general
  :config
  (general-evil-setup t)
  (general-create-definer anto/leader-key
    :keymaps '(normal insert visual emacs)
    :prefix "SPC"
    :global-prefix "C-SPC"))

(anto/leader-key
  ;; t 
  "t"  '(:ignore t :which-key "theme")
  "tt" '(counsel-load-theme :which-key "choose theme")
  ;; f --> opening specific file
  "f"  '(:ignore t :which-key "file")
  "fe" '((lambda () (interactive) (find-file (expand-file-name (concat user-emacs-directory "config.org")))) :which-key "emacs")
  "fx" '((lambda () (interactive) (find-file (expand-file-name "~/.xmonad/xmonad.org" ))) :which-key "xmonad")
  "fo" '((lambda () (interactive) (find-file (expand-file-name "~/.config/openbox/rc.xml"))) :which-key "openbox"))

(anto/leader-key
 "b"  '(:ignore t :which-key "buffer")
 "bb" 'switch-to-buffer
 "bk" 'kill-buffer)

(anto/leader-key
 "o"  '(:ignore t :which-key "open")
 "ot" 'eshell)

(anto/leader-key
  "w" '(:ignore t :which-key "window")
  "ww" 'evil-window-next
  "wh" 'evil-window-left
  "wj" 'evil-window-down
  "wk" 'evil-window-up
  "wl" 'evil-window-right
  "w=" 'balance-windows
  "w." 'evil-window-increase-width
  "w," 'evil-window-decrease-width
  "wv" 'evil-window-vsplit
  "ws" 'evil-window-split
  "wc" 'evil-window-delete
  "w+" 'evil-window-increase-height
  "w-" 'evil-window-decrease-height:w)

(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scrool t)
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)
  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package all-the-icons)

(use-package command-log-mode
  :commands command-log-mode)

(use-package doom-themes ;)
  :init (load-theme 'doom-rouge t))

(use-package doom-modeline
  :init (doom-modeline-mode 1)
  :custom  ((doom-modeline-height 15)))

(use-package which-key
  :defer 0
  :diminish which-key-mode
  :config
  (which-key-mode)
  (setq which-key-idle-delay 0.5))

(use-package vertico
  :bind (
	 :map minibuffer-local-map
	      ("C-j" . vertico-next)
	      ("C-k" . vertico-previous))
  :init
  (vertico-mode)
  (setq vertico-resize t)
  (setq vertico-cycle t))

(use-package orderless
  :init
  (setq orderless-style-dispatchers '(+orderless-dispatch))
  (setq completion-styles '(orderless)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))))

(use-package savehist
  :init
  (savehist-mode))

(use-package marginalia
  :bind (("M-A" . marginalia-cycle)
         :map minibuffer-local-map
         ("M-A" . marginalia-cycle))
  :init
  (marginalia-mode))

(use-package counsel
  :bind (("C-M-j" . 'counsel-switch-buffer)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history))
  :custom
  (counsel-linux-app-format-function #'counsel-linux-app-format-function-name-only)
  :config
  (counsel-mode 1))

(use-package helpful
  :commands (helpful-callable helpful-variable helpful-command helpful-key)
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

(use-package helpful
  :commands (helpful-callable helpful-variable helpful-command helpful-key)
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package rainbow-mode
  :config
  (add-hook 'prog-mode-hook #'rainbow-mode)
  (add-hook 'org-mode-hook #'rainbow-mode))

(use-package beacon
  :config
  (beacon-mode 1))

(use-package vterm
  :init
  (setq vterm-module-cmake-args "-DUSE_SYSTEM_LIBVTERM=no")
  :commands vterm
  :config
  (setq term-prompt-regexp "^[^#$%>\n]*[#$%>] *")  ;; Set this to match your custom shell prompt
  ;;(setq vterm-shell "zsh")                       ;; Set this to customize the shell to launch
  (setq vterm-max-scrollback 10000))

(use-package vterm-toggle
  :bind
  (("<f2>"  . vterm-toggle))
  (("C-<f2>" . vterm-toggle-cd)))

;;(use-package amx)
;;(global-set-key (kbd "M-x") 'amx)

(use-package dired
  :ensure nil
  :commands (dired dired-jump)
  :bind (("C-x C-j" . dired-jump))
  :custom ((dired-listing-switches "-agho --group-directories-first"))
  :config
  (evil-collection-define-key 'normal 'dired-mode-map
    "h" 'dired-single-up-directory
    "l" 'dired-single-buffer )) 

(use-package dired-single)

(use-package all-the-icons-dired
  :hook (dired-mode . all-the-icons-dired-mode))

(use-package dired-open
  :config
  (setq dired-open-extensions '(("png" . "feh")
                                ("pdf" . "zathura")
				    ("mp4" . "mpv")
				    ("gif" . "feh")
                                ("jpg" . "feh")
                                ("mkv" . "mpv"))))

(use-package dired-hide-dotfiles
  :hook (dired-mode . dired-hide-dotfiles-mode)
  :bind (("C-c h" . dired-hide-dotfiles-mode))
  :config
  (evil-collection-define-key 'normal 'dired-mode-map
    "H" 'dired-hide-dotfiles-mode)) 

(anto/leader-key
 "." 'dired-jump)

(use-package yasnippet
  :config
  (setq yas-snippet-dirs '("~/dotfiles/emacs/myEmacs/snippets"))
  (yas-global-mode 1))

(use-package centaur-tabs
  :demand
  :config
  (centaur-tabs-mode t)
  (setq centaur-tabs-style "rounded")
  (setq centaur-tabs-height 32)
  (setq centaur-tabs-set-icons t)
  (setq centaur-tabs-set-bar 'under)
  (setq centaur-tabs-set-modified-marker t)
  :bind
  ("<C-iso-lefttab>" . centaur-tabs-backward)
  ("C-<tab>" . centaur-tabs-forward)
  (:map evil-normal-state-map
	     ("g t" . centaur-tabs-forward)
	     ("g T" . centaur-tabs-backward)))

(use-package browse-kill-ring
  :bind (("M-y" . browse-kill-ring)))

(use-package page-break-lines
  :ensure t
  :diminish (page-break-lines-mode))

(use-package dashboard
  :init
  (progn
    (setq dashboard-items '((recents   . 5)
                            (bookmarks . 10)))
    (setq dashboard-show-shortcuts nil)
    (setq dashboard-center-content nil)
    (setq dashboard-banner-logo-title "EMACS")
    (setq dashboard-set-files-icons t)
    (setq dashboard-set-heading-icons t)
    (setq dashboard-startup-banner (concat user-emacs-directory "image/emacs_logo_lofi.png"))
    (setq dashboard-set-navigator t))
  :config
  (dashboard-setup-startup-hook))

(use-package eyebrowse
  :init
  (eyebrowse-mode t))

(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :init
  (setq lsp-keymap-prefix "C-c l")  ;; Or 'C-l', 's-l'
  :hook
  ((lsp-mode . lsp-enable-which-key-integration))
  :config
  (lsp-enable-which-key-integration t))

(use-package company
  :after lsp-mode
  :hook (prog-mode . company-mode)
  :bind (:map company-active-map
         ("<tab>" . company-complete-selection))
        (:map lsp-mode-map
         ("<tab>" . company-indent-or-complete-common))
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0))

(use-package company-box
  :hook (company-mode . company-box-mode))

(use-package lsp-ui
  :hook (lsp-mode . lsp-ui-mode)
  :config
  (setq lsp-ui-doc-position 'bottom)
  (setq lsp-ui-sideline-enable nil)
  (setq lsp-ui-sideline-show-hover nil))

(use-package lsp-ivy
  :after lsp-mode)

(use-package evil-nerd-commenter
  :bind ("M-/" . evilnc-comment-or-uncomment-lines))

(use-package dap-mode
  :after lsp-mode
  :config
  (dap-auto-configure-mode))

(add-hook 'c++-mode-hook 'irony-mode)
(add-hook 'c-mode-hook 'irony-mode)
(add-hook 'objc-mode-hook 'irony-mode)

(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

(add-hook 'sh-mode-hook 'lsp-mode)

(use-package projectile)
(use-package flycheck)
(use-package hydra)
(use-package lsp-java :config (add-hook 'java-mode-hook 'lsp))
(use-package dap-java :ensure nil)

(use-package org
  :config
  (setq org-ellipsis " ▾"))

(setq org-todo-keywords
    '((sequence "TODO" "IN-PROGRESS" "WAITING" "DONE")))

(require 'org-tempo)

(global-set-key (kbd "C-c a") 'org-agenda)
(setq org-agenda-files '("~/Documents/org/agenda/agenda.org"))

(setq org-log-mode t)

(use-package org-superstar
  :config
  (add-hook 'org-mode-hook (lambda () (org-superstar-mode 1))))
  
(setq org-startup-folded t)
(setq org-startup-indented t)
(setq org-startup-with-inline-images t)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (eshell     . t)
   (C          . t)
   (awk        . t)
   (sed        . t)
   (shell      . t)
   (haskell    . t)
   (java       . t)
   (plantuml   . t)))

(setq org-confirm-babel-evaluate nil
      org-src-preserve-indentation t)

(use-package hide-mode-line)

(defun anto/presentation-setup ()
  (hide-mode-line-mode 1)
  (org-display-inline-images)
  (setq text-scale-mode-amount 2)
  (text-scale-mode 1))

(defun anto/presentation-end ()
  (hide-mode-line-mode 0)
  (text-scale-mode 0))

(use-package org-tree-slide
  :bind
  (("<f5>" . org-tree-slide-mode))
  :hook
  ((org-tree-slide-play . anto/presentation-setup)
   (org-tree-slide-stop . anto/presentation-end))
  :custom
  (org-tree-slide-slide-in-effect t)
  (org-tree-slide-activate-message "Presentation started!")
  (org-tree-slide-deactivate-message "Presentation ended!")
  (org-tree-slide-header t)
  (org-tree-slide-breadcrumbs " > ")
  (org-image-actual-width nil))

(setq gc-cons-threshold (* 10 1024 1024))
