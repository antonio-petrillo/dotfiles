(("default" . ((user-emacs-directory . "~/dotfiles/emacs/myEmacs")))
 ("minimal" . ((user-emacs-directory . "~/dotfiles/emacs/minimal")))
 ("doom"    . ((user-emacs-directory . "~/dotfiles/emacs/emacs-doom")
	       (env . (("DOOMDIR" . "~/dotfiles/emacs/.doom.d"))))))
