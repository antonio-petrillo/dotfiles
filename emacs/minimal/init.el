(setq gc-cons-threshold (* 100 1024 1024))

;; Init.Elialize package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(require 'use-package)
(setq use-package-always-ensure t)

(setq inhibit-startup-message t) ;; disabling the startup buffer
;; (scroll-bar-mode -1)             ;; disable scrollbar
(menu-bar-mode -1)               ;; disable menubar
(tool-bar-mode -1)               ;; disable toolbar
(tooltip-mode -1)                ;; disable tooltips 
(set-fringe-mode 5)

;; (setq visible-bell nil)            ;; disabling annoying

(column-number-mode)
(global-display-line-numbers-mode t)


(set-face-attribute 'default nil :font "Fira Code Retina" :height 120)

;; Set the fixed pitch face
(set-face-attribute 'fixed-pitch nil :font "Fira Code Retina" :height 120)

;; Set the variable pitch face
(set-face-attribute 'variable-pitch nil :font "Fira Code Retina" :height 140 :weight 'regular)

;; don't create lockfile, I know what file I'm gonna edit
(setq create-lockfiles nil)

;; don't make backup files
(setq make-backup-files nil)

;; username and email
(setq user-full-name "Antonio Petrillo"
      user-mail-address "antonio.petrillo4@studenti.unina.it")

;; type yes or no require to much effort
(fset 'yes-or-no-p 'y-or-n-p)

;; scroll one line at a time (less "jumpy" than defaults)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time

(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
  
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
  
(setq scroll-step 1) ;; keyboard scroll one line at a time

(use-package gruber-darker-theme
  :init
  (load-theme 'gruber-darker t))

(use-package vertico
  :init
  (vertico-mode)
  (setq vertico-enable-resize t)
  (setq vertico-cycle t))

(use-package orderless
  :init
  (setq completion-styles '(orderless)
	completion-category-defaults nil
	completion-category-overrides '((file (styles partial-completion)))))

(use-package savehist
  :init
  (savehist-mode))

(use-package marginalia
  :bind (("M-A" . marginalia-cycle)
	 :map minibuffer-local-map
	 ("M-A" . marginalia-cycle))
  :init
  (marginalia-mode))

(global-unset-key (kbd "C-x C-z"))
(global-unset-key (kbd "C-z"))
(global-unset-key (kbd "<escape>"))
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(setq vc-follow-symlinks t)

(use-package which-key
  :defer 0
  :diminish which-key-mode
  :config
  (which-key-mode)
  (setq which-key-idle-delay 0.5))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package dired
  :ensure nil
  :commands (dired dired-jump)
  :bind
  (("C-x C-j" . dired-jump))
  :custom ((dired-listing-switches "-agho --group-directories-first")))

(use-package dired-single)

(use-package dired-open
  :config
  (setq dired-open-extensions '(("png" . "feh")
				("pdf" . "zathura")
				("mp4" . "mpv")
				("gif" . "feh")
				("jpg" . "feh")
				("mkv" . "mpv"))))

(use-package dired-hide-dotfiles
  :hook (dired-mode . dired-hide-dotfiles-mode)
  :bind (("C-c h" . dired-hide-dotfiles-mode)))

(setq gc-cons-threshold (* 10 1024 1024))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(rust-mode zen-and-art-theme which-key vertico use-package rainbow-delimiters orderless marginalia gruber-darker-theme goto-chg dired-single dired-open dired-hide-dotfiles annalist)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
