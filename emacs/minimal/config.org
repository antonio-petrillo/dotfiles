* Startup
Expanding the threshold of garbage collector for a faster startup.
#+begin_src emacs-lisp :tangle init.el
(setq gc-cons-threshold (* 100 1024 1024))
#+end_src
* Early Init
** Package manager setup
Enabling *MELPA* and *Org* repository besides *ELPA* 
#+begin_src emacs-lisp :tangle init.el
;; Init.Elialize package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(require 'use-package)
(setq use-package-always-ensure t)
#+end_src
** Basic UI config
This section is self-explanatory.
#+begin_src emacs-lisp :tangle init.el
(setq inhibit-startup-message t) ;; disabling the startup buffer
;; (scroll-bar-mode -1)             ;; disable scrollbar
(menu-bar-mode -1)               ;; disable menubar
(tool-bar-mode -1)               ;; disable toolbar
(tooltip-mode -1)                ;; disable tooltips 
(set-fringe-mode 5)

;; (setq visible-bell nil)            ;; disabling annoying

(column-number-mode)
(global-display-line-numbers-mode t)


(set-face-attribute 'default nil :font "Fira Code Retina" :height 120)

;; Set the fixed pitch face
(set-face-attribute 'fixed-pitch nil :font "Fira Code Retina" :height 120)

;; Set the variable pitch face
(set-face-attribute 'variable-pitch nil :font "Fira Code Retina" :height 140 :weight 'regular)
#+end_src
** Basic UX config
Also this section is itself explanatory.
#+begin_src emacs-lisp :tangle init.el
;; don't create lockfile, I know what file I'm gonna edit
(setq create-lockfiles nil)

;; don't make backup files
(setq make-backup-files nil)

;; username and email
(setq user-full-name "Antonio Petrillo"
      user-mail-address "antonio.petrillo4@studenti.unina.it")

;; type yes or no require to much effort
(fset 'yes-or-no-p 'y-or-n-p)

;; scroll one line at a time (less "jumpy" than defaults)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time

(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
  
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
  
(setq scroll-step 1) ;; keyboard scroll one line at a time
#+end_src
** Change default theme
#+BEGIN_SRC emacs-lisp :tangle init.el
  (use-package gruber-darker-theme
    :init
    (load-theme 'gruber-darker t))
 #+END_SRC
** Vertico & Marginalia
#+BEGIN_SRC emacs-lisp :tangle init.el
  (use-package vertico
    :init
    (vertico-mode)
    (setq vertico-enable-resize t)
    (setq vertico-cycle t))

  (use-package orderless
    :init
    (setq completion-styles '(orderless)
	  completion-category-defaults nil
	  completion-category-overrides '((file (styles partial-completion)))))

  (use-package savehist
    :init
    (savehist-mode))

  (use-package marginalia
    :bind (("M-A" . marginalia-cycle)
	   :map minibuffer-local-map
	   ("M-A" . marginalia-cycle))
    :init
    (marginalia-mode))
#+END_SRC
** Overriding some frustrating keybinding
The =C-x C-z= minimize emacs and that is frustrating by itself,
but when I use a system with tiling window manager it's even more frustrating.
#+BEGIN_src emacs-lisp :tangle init.el
(global-unset-key (kbd "C-x C-z"))
(global-unset-key (kbd "C-z"))
(global-unset-key (kbd "<escape>"))
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(setq vc-follow-symlinks t)
#+END_SRC
* Normal config
** Which key
#+begin_src emacs-lisp :tangle init.el
(use-package which-key
  :defer 0
  :diminish which-key-mode
  :config
  (which-key-mode)
  (setq which-key-idle-delay 0.5))
#+end_src
** Rainbow delimiters
#+begin_src emacs-lisp :tangle init.el
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))
#+end_src
** Dired
#+BEGIN_SRC emacs-lisp :tangle init.el
  (use-package dired
    :ensure nil
    :commands (dired dired-jump)
    :bind
    (("C-x C-j" . dired-jump))
    :custom ((dired-listing-switches "-agho --group-directories-first")))

  (use-package dired-single)

  (use-package dired-open
    :config
    (setq dired-open-extensions '(("png" . "feh")
				  ("pdf" . "zathura")
				  ("mp4" . "mpv")
				  ("gif" . "feh")
				  ("jpg" . "feh")
				  ("mkv" . "mpv"))))

  (use-package dired-hide-dotfiles
    :hook (dired-mode . dired-hide-dotfiles-mode)
    :bind (("C-c h" . dired-hide-dotfiles-mode)))
#+END_SRC
* End config
Resize garbage collector threshold to a more appropriate size.
#+begin_src emacs-lisp :tangle init.el
(setq gc-cons-threshold (* 10 1024 1024))
#+end_src
