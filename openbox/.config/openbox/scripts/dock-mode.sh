#!/usr/bin/env bash

# The script to setup docked/laptop config automatically setup docked/laptop config, so....
# The next snippet can be used someday
#
#dock=$(echo -e "laptop\ndocked" | dmenu)
#echo $dock
#if [ -n $dock ]; then
#    if [ $dock = "laptop" ]; then
#        # lauch laptop config
#    else
#        # launch docked config
#    fi
#fi

$HOME/.config/openbox/scripts/screen-layour.sh
$HOME/.config/polybar/launch.sh
