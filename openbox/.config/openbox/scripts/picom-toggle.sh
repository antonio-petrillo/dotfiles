#!/usr/bin/env bash

if pgrep -x "picom" > /dev/null
then
    killall picom
else
    picom $HOME/.config/openbox/picom.conf &
fi
